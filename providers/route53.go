package providers

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awsRoute53 "github.com/aws/aws-sdk-go/service/route53"
	"github.com/aws/aws-sdk-go/service/servicediscovery"
	"net"
)

type Route53 struct {
	ProviderName string
	zoneid       string
	domainName   string
	awsConfig    *awsConfig
}

type awsConfig struct {
	sess    *session.Session
	route53 *awsRoute53.Route53
}

func NewRoute53Provider(zoneid string, domainName string) *Route53 {
	s, _ := session.NewSession()

	awc := &awsConfig{
		sess:    s,
		route53: awsRoute53.New(s),
	}
	r := &Route53{
		ProviderName: "route53",
		zoneid:       zoneid,
		domainName:   domainName,
		awsConfig:    awc,
	}
	return r
}

func (r Route53) NewServiceDiscoveryClient() {
	sdc := servicediscovery.New(r.awsConfig.sess, aws.NewConfig().WithRegion("us-east-1"))
	s := &servicediscovery.ListNamespacesInput{}
	out, _ := sdc.ListNamespaces(s)
	for _, r := range out.Namespaces {
		fmt.Println(r)
		id := r.Id
		s := &servicediscovery.DeleteNamespaceInput{
			Id: id,
		}
		sdc.DeleteNamespace(s)

	}
	fmt.Println(*out)
}
func (r Route53) Name() string {
	return r.ProviderName
}
func (r Route53) RegisterService(servicename string, ipaddr net.IP) error {
	serviceRecords, _ := r.findServiceRecords("A", servicename)
	if !ipInSlice(ipaddr, serviceRecords) {
		serviceRecords = append(serviceRecords, ipaddr)
		err := r.updateRecords("UPSERT", servicename, serviceRecords)
		if err != nil {
			return err
		}
	}
	return nil
}

func (r Route53) DeregisterService(servicename string, ipaddr net.IP) error {
	return nil
}

func (r Route53) RemoveService(servicename string) error {
	serviceRecords, _ := r.findServiceRecords("A", servicename)
	err := r.updateRecords("DELETE", servicename, serviceRecords)
	if err != nil {
		return err
	}
	return nil
}

func (r Route53) domainforService(servicename string) string {
	dns := servicename + "." + r.domainName + "."
	return dns
}

func ipInSlice(ip net.IP, list []net.IP) bool {
	for _, v := range list {
		if v.Equal(ip) {
			return true
		}
	}
	return false
}

func createResourceRecords(ipAddrs []net.IP) []*awsRoute53.ResourceRecord {
	var rs []*awsRoute53.ResourceRecord
	for _, ip := range ipAddrs {
		ipString := ip.String()
		r := &awsRoute53.ResourceRecord{
			Value: &ipString,
		}
		rs = append(rs, r)
	}
	return rs

}

func (r Route53) updateRecords(action string, servicename string, serviceRecords []net.IP) error {

	resourceRecords := createResourceRecords(serviceRecords)

	crsi := &awsRoute53.ChangeResourceRecordSetsInput{
		ChangeBatch: &awsRoute53.ChangeBatch{ // Required
			Changes: []*awsRoute53.Change{ // Required
				{ // Required
					Action: aws.String(action), // Required
					ResourceRecordSet: &awsRoute53.ResourceRecordSet{ // Required
						Name:            aws.String(r.domainforService(servicename)), // Required
						Type:            aws.String("A"),                             // Required
						ResourceRecords: resourceRecords,
						TTL:             aws.Int64(int64(60)),
						Weight:          aws.Int64(int64(1)),
						SetIdentifier:   aws.String("Updating record set"),
					},
				},
			},
		},
		HostedZoneId: aws.String(r.zoneid), // Required
	}

	_, err := r.awsConfig.route53.ChangeResourceRecordSets(crsi)
	if err != nil {
		return err
	}

	return nil
}

func (r Route53) findServiceRecords(dnsType string, serviceName string) ([]net.IP, error) {
	//records := make(map[string][]*awsRoute53.ResourceRecord)
	var ipAddrs []net.IP

	lri := &awsRoute53.ListResourceRecordSetsInput{
		HostedZoneId: aws.String(r.zoneid),
	}

	rL, err := r.awsConfig.route53.ListResourceRecordSets(lri)
	if err != nil {
		panic(err)
	}

	for _, recordSet := range rL.ResourceRecordSets {
		//values := []*awsRoute53.ResourceRecord{}
		if *recordSet.Type == dnsType && *recordSet.Name == r.domainforService(serviceName) {
			for _, resourceRecord := range recordSet.ResourceRecords {
				ipAddrs = append(ipAddrs, net.ParseIP(*resourceRecord.Value))
			}

		}

	}

	return ipAddrs, nil
}
