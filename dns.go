package main

import (
	"bitbucket.org/frostick/nomad-route53/providers"
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/hashicorp/consul/api"
	"log"
	"net"
	"os"
	"strings"
)

type DNSProvider interface {
	RegisterService(servicename string, ipaddress net.IP) error
	DeregisterService(servicename string, ipaddress net.IP) error
	RemoveService(servicename string) error
	Name() string
}

type ConsulWatch struct {
	input       map[string][]string
	agent       *api.Agent
	kv          *api.KV
	session     *api.Session
	catalog     *api.Catalog
	dnsProvider DNSProvider
}

var node string

func main() {
	providerFlag := flag.String("dns", "route53", "select dns provider")
	flag.Parse()
	var dnsProvider DNSProvider

	if *providerFlag == "route53" {
		zoneid := os.Getenv("AWS_ROUTE53_ZONEID")
		domain := os.Getenv("AWS_ROUTE53_DOMAIN")
		if len(zoneid) == 0 {
			fmt.Println("No AWS zone id found, set AWS_ROUTE53_ZONEID")
			os.Exit(1)
		}
		if len(domain) == 0 {
			fmt.Println("No AWS domain found, set AWS_ROUTE53_DOMAIN")
			os.Exit(1)
		}
		dnsProvider = providers.NewRoute53Provider(zoneid, domain)
	}
	scanner := bufio.NewScanner(os.Stdin)
	var input string
	for scanner.Scan() {
		input = scanner.Text()
	}
	cw, err := NewConsulWatch([]byte(input), dnsProvider)

	if err != nil {
		panic(err)
	}
	node, _ = cw.agent.NodeName()

	if cw.AcquireLeadership() {
		log.Printf("%s is leader", node)
		registeredServices := cw.RegisteredServices()
		cw.PurgeServices(registeredServices)
		cw.RegisterNewServices()
	}

}

func parseJSON(jsn []byte) (map[string][]string, error) {
	c := make(map[string][]string)
	err := json.Unmarshal(jsn, &c)
	if err != nil {
		return nil, err
	}
	return c, nil

}
func (cw ConsulWatch) RegisteredServices() []string {
	var rs []string
	rs, _, _ = cw.kv.Keys("service/nomad-route53/services", "", nil)
	return rs
}

func NewConsulWatch(input []byte, dnsProvider DNSProvider) (*ConsulWatch, error) {
	config := api.DefaultConfig()
	config.Token = os.Getenv("CONSUL_TOKEN")
	if len(config.Token) == 0 {
		return nil, errors.New("Consul token not found")
	}
	consul, err := api.NewClient(config)
	if err != nil {
		return nil, err
	}
	watchJson, err := parseJSON(input)
	if err != nil {
		return nil, err
	}
	cw := &ConsulWatch{
		input:       watchJson,
		agent:       consul.Agent(),
		kv:          consul.KV(),
		session:     consul.Session(),
		catalog:     consul.Catalog(),
		dnsProvider: dnsProvider,
	}
	return cw, nil
}

func (cw ConsulWatch) AcquireLeadership() bool {
	s, _, err := cw.session.Create(nil, nil)
	defer cw.session.Destroy(s, nil)

	if err != nil {
		return false
	}

	key := &api.KVPair{
		Key:     "service/nomad-route53/leader",
		Session: s,
	}
	_, _, err = cw.kv.Acquire(key, nil)
	if err != nil {
		fmt.Println(err)
		return false
	}

	return true
}

func (cw ConsulWatch) PurgeServices(currentServices []string) {
	for _, rs := range currentServices {
		serviceName := extractServiceFromKey(rs)
		//service existing in keystore but not registered on consul
		//this is an old service so we can remove
		if _, ok := cw.input[serviceName]; !ok {
			cw.dnsProvider.RemoveService(serviceName)
			key := fmt.Sprintf("service/nomad-route53/services/%s", serviceName)
			_, err := cw.kv.DeleteTree(key, nil)
			if err != nil {
				log.Printf("%s - failed to purge service: %s, reason: %s", serviceName, err)
			} else {
				log.Printf("%s - purging service: %s", node, serviceName)
			}
		}

	}
}

func extractServiceFromKey(key string) string {
	//format will be service/nomad-route53/services/<SERVICE>/<KEY>
	s := strings.Split(key, "/")
	return s[3]

}

func (cw ConsulWatch) RegisterNewServices() {
	services := make(map[string]map[string]string)

	for service, tags := range cw.input {
		tagMap := make(map[string]string)
		services[service] = tagMap
		for _, tag := range tags {
			//this allows us to get component parts of traefik tags
			compTag := strings.Split(tag, "=")
			if len(compTag) == 2 {
				services[service][compTag[0]] = compTag[1]
			} else {
				services[service][tag] = ""
			}
		}
	}
	for serviceName, tagMap := range services {
		if _, ok := tagMap[cw.dnsProvider.Name()]; ok {
			if rule, ok := tagMap["traefik.frontend.rule"]; ok {
				//example rule traefik.frontend.rule=Host:minio.prob.es
				//we need away of extracting just minio from this example as this
				//is the actualy service name we want to register in the dns if
				// the tag traefik.frontend.rule does not exist we can just use the
				// servicename from consul
				hostname := strings.Split(rule, ":")
				sp := strings.Split(hostname[1], ".")
				serviceName = sp[0]
			}
			//there can be multiple services of the same type as we may be load balancing
			//across nomad nodes or even running multiple instances on the same node
			serviceInstances, _, _ := cw.catalog.Service(serviceName, "", nil)

			//when we register a service in dns we also register the ips in consul this saves
			//having to do a dns lookup each tie to get all the current service ips registered
			serviceIPs, _ := cw.ServiceIPs(serviceName)
			//each service will run on a node, each node has a public facing ip address
			//we need to find the node ip address that are not currently registered in consul
			missingPublicIPs := cw.UnregisteredPublicIPs(serviceInstances, serviceIPs)

			for _, ip := range missingPublicIPs {
				err := cw.dnsProvider.RegisterService(serviceName, ip)
				if err == nil {
					//json encode the array to store in consul kv, we may want a more
					//complicated data struct at a future point
					jsp, _ := json.Marshal(missingPublicIPs)
					key := fmt.Sprintf("service/nomad-route53/services/%s/ip", serviceName)
					log.Printf("%s -  registering service: %s", node, serviceName)
					cw.PutKey(key, jsp)
				}

			}
		}
	}
}

func ipMap(ipAddrs []string) map[string]struct{} {
	ipMap := make(map[string]struct{})
	for _, ip := range ipAddrs {
		var empty struct{}
		ipMap[ip] = empty
	}
	return ipMap
}

func (cw ConsulWatch) PutKey(key string, value []byte) error {
	k := &api.KVPair{Key: key, Value: value}
	_, err := cw.kv.Put(k, nil)
	if err != nil {
		return err
	}
	return nil
}

func (cw ConsulWatch) UnregisteredPublicIPs(cs []*api.CatalogService, serviceIPs []string) []net.IP {
	serviceIPMap := ipMap(serviceIPs)
	var ipAddrs []net.IP
	missingMap := make(map[string]struct{})

	for _, s := range cs {
		if _, ok := serviceIPMap[s.NodeMeta["public_wan_address"]]; !ok {
			var empty struct{}
			//this stops us ending up with duplicate wan addresses if two instances of a service
			//are running on the same node
			missingMap[s.NodeMeta["public_wan_address"]] = empty
		}

	}
	for v, _ := range missingMap {
		ip := net.ParseIP(v)
		ipAddrs = append(ipAddrs, ip)
	}
	return ipAddrs

}
func (cw ConsulWatch) ServiceIPs(serviceName string) ([]string, error) {
	key := fmt.Sprintf("service/nomad-route53/services/%s/ip", serviceName)
	si, _, err := cw.kv.Get(key, nil)
	if err != nil {
		return nil, err
	}
	ipAddrs := []string{}
	if si != nil {
		err = json.Unmarshal(si.Value, &ipAddrs)
		if err != nil {
			return nil, err
		}
	}
	return ipAddrs, nil

}
